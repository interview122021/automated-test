FROM ubuntu:16.04
LABEL version="1.0.0"
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y && \
    apt-get install apache2 apache2-utils curl -y && \
    apt-get clean
COPY index.html /var/www/html
EXPOSE 80
CMD ["apachectl", "-D", "FOREGROUND]